$(document).on('submit', '#formid', function(e){
    e.preventDefault();

    $.ajax({
        url: "/acc/register",
        type: "POST",
        data: {
            'nama':$('#nama').val(),
            'lokasi':$('#loc').val(),
            'nomor_telepon':$('#tel').val(),
            'vaksin_ada':$('#va').val(),
            'vaksin_tersedia':$('#vac').val(),
            'bed_ada':$('#be').val(),
            'bed_tersedia':$('#bed').val(),

            'username':$('#floatingInputUsername').val(),
            'email':$('#floatingInputEmail').val(),
            'password1':$('#floatingPassword').val(),
            'password2':$('#floatingPasswordConfirm').val(),

            'csrfmiddlewaretoken':$('input[name=csrfmiddlewaretoken]').val()
        },
        dataType: 'json',
        success : function(response_data) {
            
            $('#war-div').empty();
            $('#war-div').append(response_data['result']);
            $('#war-div').show();

            console.log("success"); // another sanity check
        }
    })
})