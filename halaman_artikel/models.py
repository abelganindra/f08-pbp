from django.db import models

# Create your models here.
class Artikel(models.Model):
    penulis = models.CharField(max_length=1000)
    asal_rumah_sakit = models.CharField(max_length=1000)
    judul = models.CharField(max_length=1000)
    tanggal_pembuatan = models.DateField(auto_now_add=True)
    isi = models.TextField(default="Isi artikel")
