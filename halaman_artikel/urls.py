from django.conf.urls import include
from django.urls import path
from .views import daftar_artikel, tambah_artikel, ArtikelViewSet
from rest_framework import routers


router = routers.DefaultRouter()
router.register(r'artikel', ArtikelViewSet)

urlpatterns = [
    path('', daftar_artikel, name='daftar_artikel'),
    path('add-article/', tambah_artikel, name='tambah_artikel'),
    path('', include(router.urls)),
]