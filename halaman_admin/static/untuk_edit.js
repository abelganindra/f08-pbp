$(document).on('submit', '#edit_profile', function(e){
    e.preventDefault();
    console.log('TES')

    $.ajax({
        url: "/profile/edit-profile/",
        type: "POST",
        data: {
            'nama':$('#nama').val(),
            'lokasi':$('#lokasi').val(),
            'nomor_telepon':$('#nomor_telepon').val(),
            'vaksin_tersedia':$('#vaksin_tersedia').val(),
            'bed_tersedia':$('#bed_tersedia').val(),
            'csrfmiddlewaretoken':$('input[name=csrfmiddlewaretoken]').val()
        },
        dataType: 'json',
        success : function(response_data) {
            if(response_data['berhasil']){
                $('#gagal').empty();
                $('#berhasil').empty();
                $('#berhasil').append(response_data['hasil']);
                $('#gagal').hide();
                $('#berhasil').show();
            }
            else{
                $('#gagal').empty();
                $('#berhasil').empty();
                $('#gagal').append(response_data['hasil']);
                $('#berhasil').hide();
                $('#gagal').show();
            }
        }
    })
})