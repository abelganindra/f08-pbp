from django.shortcuts import render, redirect
from .models import RumahSakit
from django.http.response import HttpResponse, JsonResponse
from django.contrib.auth.decorators import login_required
from .forms import EditForm

@login_required(login_url='login')
def rs_info(request):
    if request.user.is_superuser:
        return redirect('/admin/')

    else:
        rs = RumahSakit.objects.all()
        rs_target = None
        for rumah_sakit in rs:
            if rumah_sakit.user == request.user:
                rs_target = rumah_sakit
                break
        
        response = {'rs_target': rs_target}
        return render(request, 'informasi.html', response)

@login_required(login_url='login')
def edit_profile(request):
    if request.user.is_superuser:
        return redirect('/admin/')

    else:
        form = EditForm()
        rs = RumahSakit.objects.all()
        rs_target = None

        for rumah_sakit in rs:
            if rumah_sakit.user == request.user:
                rs_target = rumah_sakit
                break

        if request.method == 'POST':
            # form = EditForm(request.POST, instance=rs_target)
            # if form.is_valid():
            #     form.save()
            #     return redirect('/profile/')
            form = EditForm(request.POST or None, instance=rs_target);

            response_data = {}

            if form.is_valid():
                form.save()
                response_data['hasil'] = 'Profil berhasil diubah!'
                response_data['berhasil'] = True
                return JsonResponse(response_data)

            else:
                response_data['hasil'] = 'Terdapat kesalahan dalam memasukkan data! Silakan cek kembali'
                response_data['berhasil'] = False
                return JsonResponse(response_data)

        response = {'form':form, 'rs_target':rs_target}
        return render(request, 'edit.html', response)
