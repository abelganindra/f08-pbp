from django.forms import ModelForm
from halaman_admin.models import RumahSakit
from django import forms

class EditForm(ModelForm):
    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #     self.fields['nama'].widget.attrs.update({
    #         'required':'',
    #         'name':'nama',
    #         'id':'To',
    #         'type':'text',
    #         'class':'form-input',
    #         'placeholder':'Silakan isi nama Anda',
    #         'maxlength':'100',
    #         'minlength':'1'
    #     })
    #     self.fields['lokasi'].widget.attrs.update({
    #         'required':'',
    #         'name':'lokasi',
    #         'id':'From',
    #         'type':'text',
    #         'class':'form-input',
    #         'placeholder':'Silakan isi lokasi',
    #         'maxlength':'100',
    #         'minlength':'1'
    #     })
    #     self.fields['nomor_telepon'].widget.attrs.update({
    #         'required':'',
    #         'name':'nomor_telepon',
    #         'id':'From',
    #         'type':'text',
    #         'class':'form-input',
    #         'placeholder':'Silakan isi nomor telepon',
    #         'maxlength':'100',
    #         'minlength':'1'
    #     })
    #     self.fields['vaksin_ada'].widget.attrs.update({
    #         'required':'',
    #         'name':'vaksin_ada',
    #         'id':'Title',
    #         'type':'text',
    #         'class':'form-input',
    #         'placeholder':'Silakan isi jumlah vaksin yang ada',
    #         'maxlength':'100',
    #         'minlength':'1'
    #     })
    #     self.fields['vaksin_tersedia'].widget.attrs.update({
    #         'required':'',
    #         'name':'vaksin_tersedia',
    #         'id':'Title',
    #         'type':'text',
    #         'class':'form-input',
    #         'placeholder':'Silakan isi jumlah vaksin yang tersedia',
    #         'maxlength':'100',
    #         'minlength':'1'
    #     })
    #     self.fields['bed_ada'].widget.attrs.update({
    #         'required':'',
    #         'name':'bed_ada',
    #         'id':'Title',
    #         'type':'text',
    #         'class':'form-input',
    #         'placeholder':'Silakan isi jumlah bed yang ada',
    #         'maxlength':'100',
    #         'minlength':'1'
    #     })
    #     self.fields['bed_tersedia'].widget.attrs.update({
    #         'required':'',
    #         'name':'bed_tersedia',
    #         'id':'Title',
    #         'type':'text',
    #         'class':'form-input',
    #         'placeholder':'Silakan isi jumlah bed yang tersedia',
    #         'maxlength':'100',
    #         'minlength':'1'
    #     })

    class Meta:
        model = RumahSakit
        fields = [
            'nama',
            'lokasi',
            'nomor_telepon',
            'vaksin_ada',
            'vaksin_tersedia',
            'bed_ada',
            'bed_tersedia',
            ]
        # widgets={
        #     'nama' : forms.TextInput(attrs={'class':'form-control'}),
        #     'lokasi' : forms.TextInput(attrs={'class':'form-control'}),
        #     'nomor_telepon' : forms.TextInput(attrs={'class':'form-control'}),
        #     'vaksin_ada' : forms.TextInput(attrs={'class':'form-control'}),
        #     'vaksin_tersedia' : forms.TextInput(attrs={'class':'form-control'}),
        #     'bed_ada' : forms.TextInput(attrs={'class':'form-control'}),
        #     'bed_tersedia' : forms.TextInput(attrs={'class':'form-control'})
        # }
        
        # def get_object(self):
        #     return self.request.user