from django.db import models
from django.contrib.auth.models import User

class RumahSakit(models.Model):

    nama = models.CharField(max_length=20, default="defaultName")
    lokasi = models.CharField(max_length=30) 
    nomor_telepon = models.CharField(max_length=20)
    vaksin_ada = models.BooleanField(default=False)
    vaksin_tersedia = models.PositiveIntegerField()
    bed_ada = models.BooleanField(default=False)
    bed_tersedia = models.PositiveIntegerField()

    rs_confirmed = models.BooleanField(default=False)
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)
    
# Create your models here.
