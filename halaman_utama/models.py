from django.db import models
from django.db.models.fields import TextField

class Masukan(models.Model):
    nama = models.CharField(max_length=20)
    email = models.EmailField()
    isi = TextField()


# Create your models here.
