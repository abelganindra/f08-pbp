# Kelompok F08
                   
| Nama | NPM |
| ------ | ------ |
| Alfatih Aditya Susanto | 2006596402 |
| Abel Ganindra | 2006597090 |
| Immanuel Brilan Solvanto Darmawan | 2006596674 |
| Abdul Ghani Suryo Adhipramono  | 2006596970  |
| Agnes Audya Tiara P  | 2006596200  |
| Avelia Diva Zahra    | 2006596176 |
 
### Cerita Aplikasi
 
Di masa pandemi COVID-19 ini, informasi mengenai ketersediaan vaksin dan kamar rawat inap yang ada di rumah sakit sekitar sangat dibutuhkan. Kami sebagai pengembang _website_ menawarkan kerja sama dengan rumah sakit dan pemerintah untuk **menampilkan informasi ketersediaan vaksin dan kamar rawat inap**. Dengan demikian, masyarakat dapat mengunjungi _website_ kami untuk melihat rumah sakit mana saja yang memiliki ketersediaan vaksin dan kamar rawat inap tanpa harus repot-repot menghubungi rumah sakit satu per satu.
 
### Manfaat Aplikasi
 
Berikut adalah manfaat yang bisa didapat oleh pengguna:
 
- Melihat informasi ketersediaan kamar perawatan
- Melihat informasi ketersediaan vaksin
- Mendapat pengetahuan seputar COVID-19 lewat artikel
 
Berikut adalah manfaat yang bisa didapat oleh pihak rumah sakit:
 
- Mempermudah publikasi perubahan informasi rumah sakit
- Meningkatkan efektivitas komunikasi antara calon pasien dengan rumah sakit.
- Mempermudah pendataan calon pasien
 
 
 
### Daftar Modul
| Nama | Modul|
| ------ | ------ |
|Abdul Ghani Suryo Adhipramono| Biodata Rumah Sakit|
|Immanuel Brilan Solvanto Darmawan|Artikel|
|Agnes Audya Tiara P|Detail informasi setiap rumah sakit|
|Abel Ganindra|Katalog Vaksin dan Bed|
|Alfatih Aditya Susanto|Login Rumah sakit|
|Avelia Diva Zahra|Homepage|
 
### Persona

Terdapat 3 pihak yang terlibat :
1. Admin website
- Mendaftarkan/menghapus rumah sakit dari daftar rumah sakit
- Mengubah status jumlah ketersediaan vaksin dan kamar rawat inap
- Melihat informasi website


2. Rumah sakit
- Mengubah biodata rumah sakit yang bersangkutan
- Melihat informasi website

 
3. Masyarakat
- Melihat informasi website

 
### Link HerokuApp

f08-pbp.herokuapp.com
 

