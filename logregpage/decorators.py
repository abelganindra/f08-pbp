from django.http import HttpResponse
from django.shortcuts import redirect

def unauthenticated_user(view_f):
    def wrapper_f(request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('info')
        else:
            return view_f(request, *args, **kwargs)
    
    return wrapper_f