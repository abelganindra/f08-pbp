from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from .decorators import unauthenticated_user
from .forms import CreateDefUserForm, CreateHospForm
from .models import RumahSakit


# Create your views here.

@unauthenticated_user
def regHosp(request):

    if request.method == 'POST':

        usern = request.POST.get('username')
        em = request.POST.get('email')
        pw1 = request.POST.get('password1')
        pw2 = request.POST.get('password2')

        nam = request.POST.get('nama')
        loc = request.POST.get('lokasi')
        tel = request.POST.get('nomor_telepon')
        va = request.POST.get('vaksin_ada')
        vac = request.POST.get('vaksin_tersedia')
        be = request.POST.get('bed_ada')
        bed = request.POST.get('bed_tersedia')

        dataUser = {
            'username':usern,
            'email':em,
            'password1':pw1,
            'password2':pw2
        }

        dataHosp = {
            'nama':nam,
            'lokasi':loc,
            'nomor_telepon':tel,
            'vaksin_ada':va,
            'vaksin_tersedia':vac,
            'bed_ada':be,
            'bed_tersedia':bed
        }

        form = CreateDefUserForm(dataUser or None)
        hosp_form = CreateHospForm(dataHosp or None)

        if form.is_valid() and hosp_form.is_valid():
            user = form.save()

            hosp = hosp_form.save(commit=False)
            hosp.user = user

            hosp.save()

            return HttpResponse('')

            
    context = {}
    return render(request, 'register1.html', context)

@unauthenticated_user
def logInHosp(request):

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_superuser:
                login(request, user)
                return redirect('info')
            else:
                rs_target = find_rs(user)
                
                if rs_target.rs_confirmed:
                    login(request, user)
                    return redirect('info')
                elif not rs_target.rs_confirmed:
                    messages.warning(request, "Rumah Sakit belum terkonfirmasi. Mohon menunggu konfirmasi kami...")
                    
        else:
            messages.warning(request, "Username atau Password salah")
    
    context = {}
    return render(request, 'login.html', context)

def logOutHosp(request):
    logout(request)
    return redirect('login')

def find_rs(user):

    rs = RumahSakit.objects.all()

    for rumah_sakit in rs:
        if rumah_sakit.user == user:
            return rumah_sakit
    
    return None