from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from .models import RumahSakit

class CreateDefUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'password1',
            'password2'
        ]
    
    def save(self, commit=True):
        user = super().save(commit=False)

        user.mail = self.cleaned_data['email']

        if commit:
            user.save()
        
        return user

class CreateHospForm(forms.ModelForm):
    class Meta:
        model = RumahSakit
        fields = [
            'nama',
            'lokasi',
            'nomor_telepon',
            'vaksin_ada',
            'vaksin_tersedia',
            'bed_ada',
            'bed_tersedia',
            ]

